FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > mosquitto.log'

COPY mosquitto .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' mosquitto
RUN bash ./docker.sh

RUN rm --force --recursive mosquitto
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD mosquitto
